# read simulated data
mydata=as.data.frame(read.table('path to mydata.txt', sep='\t', header=T))
# convert clinical, mutation, and CNA features to factors
for (j in c(1:7,49:78)){
  mydata[, j] = as.factor(mydata[, j])
}
# Split data into training and validation (7:3 stratified split based on relapse response)
library(caret)
library(h2o)
library(multiROC) # one vs all roc 
auc = data.frame(matrix(0.0, nrow = 10, ncol = 6))
colnames(auc) = c('knn', 'ann', 'lda','svm','rf','multinomial')

# convery character to numeric
temp.map = c("NoRelapse"=1, "LateRelapse"=2, "RapidRelapse"=3)

h2o.init(max_mem_size="3g")
for (i in 1:10) {
  # shuffle rows of mydata
  rand = sample(nrow(mydata))
  mydata = mydata[rand,]
  # split data into training and test
  training_index <- createDataPartition(mydata$RapidRelapse, p = .7, list = FALSE, times = 1)
  dataTrain <- mydata[ training_index,]
  dataTest  <- mydata[-training_index,]
  
  # response
  target = temp.map[as.character(dataTest$RapidRelapse)]
  NR_true = ifelse(target == 1,1,0)
  LR_true = ifelse(target == 2,1,0)
  RR_true = ifelse(target == 3,1,0)
  
  # cross validation - training scheme
  control = trainControl(method="cv", number=10,classProbs = TRUE,summaryFunction = multiClassSummary)
  
  ######################################### KNN #############################################
  # This ensures that the same resampling sets are used,which will come in handy when we compare the resampling profiles between models.
  set.seed(7)
  knn = train(RapidRelapse~., data=dataTrain, method = 'knn', trControl=control, metric = 'logLoss', tuneGrid = expand.grid(k = c(1,2,4,5,7,8,10,11,13,14))) # avoid multiples of 3
  # testing performance
  knn.pred = predict(knn, newdata = dataTest[,-1])
  # one-to-all auc
  fitted.results_knn = predict(knn, newdata = dataTest[,-1], type = "prob")
  NR_pred_knn = fitted.results_knn$NoRelapse
  LR_pred_knn = fitted.results_knn$LateRelapse
  RR_pred_knn = fitted.results_knn$RapidRelapse
  
  data.knn = data.frame(NR_true, LR_true, RR_true, NR_pred_knn, LR_pred_knn, RR_pred_knn)
  auc.knn = multi_roc(data.knn)
  auc[i,1] = auc.knn$AUC$knn$micro
  ######################################### ANN #############################################
  ann_train = as.h2o(dataTrain)
  params = list(activation=c("Rectifier","Tanh"),
                hidden=list(c(5),c(10),c(20),c(5,5),c(10,10),c(20,20),c(5,5,5),c(10,10,10),
                            c(20,20,20)),
                input_dropout_ratio=c(0,0.1,0.2),
                l1=seq(0,1e-4,1e-5),
                l2=seq(0,1e-4,1e-5))
  criteria = list(strategy = "RandomDiscrete",
                  max_models = 100,
                  stopping_tolerance=1e-2,
                  stopping_metric = "misclassification")
  # random train grid
  dl.random.grid <- h2o.grid(grid_id = paste("dl_grid",i,sep =""),
                             algorithm="deeplearning",
                             training_frame = ann_train,
                             x = 2:78,
                             y = 1,
                             seed=7,
                             hyper_params = params,
                             search_criteria = criteria,
                             # because the example data is limited in size, we use a lower fold                                number here
                             # nfolds = 10 was used for the real data set
                             nfolds=3)
  grid <- h2o.getGrid(grid_id = paste("dl_grid",i,sep =""),
                      sort_by="logLoss",
                      decreasing=FALSE)
  ann.best_model <- h2o.getModel(grid@model_ids[[1]]) # lowest logLoss
  # Predict Values
  fitted.results_ann = h2o.predict(ann.best_model, newdata = as.h2o(dataTest[,-1]))
  NR_pred_ann = as.vector(fitted.results_ann$NoRelapse)
  LR_pred_ann = as.vector(fitted.results_ann$LateRelapse)
  RR_pred_ann = as.vector(fitted.results_ann$RapidRelapse)
  
  h2o.removeAll()
  
  data.ann = data.frame(NR_true, LR_true, RR_true, NR_pred_ann, LR_pred_ann, RR_pred_ann)
  auc.ann = multi_roc(data.ann)
  auc[i,2] = auc.ann$AUC$ann$micro
  ######################################### LDA #############################################
  set.seed(7)
  lda = train(RapidRelapse~., data=dataTrain, method = 'lda',trControl=control, metric = 'logLoss')
  # testing performance
  lda.pred = predict(lda, newdata = dataTest[,-1])
  # one-to-all auc
  fitted.results_lda = predict(lda, newdata = dataTest[,-1], type = "prob")
  NR_pred_lda = fitted.results_lda$NoRelapse
  LR_pred_lda = fitted.results_lda$LateRelapse
  RR_pred_lda = fitted.results_lda$RapidRelapse
  
  data.lda = data.frame(NR_true, LR_true, RR_true, NR_pred_lda, LR_pred_lda, RR_pred_lda)
  auc.lda = multi_roc(data.lda)
  auc[i,3] = auc.lda$AUC$lda$micro
  ######################################### SVM #############################################
  set.seed(7)
  svm = train(RapidRelapse~., data=dataTrain, method = 'svmRadial',trControl=control, metric = 'logLoss', tuneLength = 10)
  
  # one-to-all auc
  fitted.results_svm = predict(svm, newdata = dataTest[,-1], type = "prob")
  NR_pred_svm = fitted.results_svm$NoRelapse
  LR_pred_svm = fitted.results_svm$LateRelapse
  RR_pred_svm = fitted.results_svm$RapidRelapse
  
  data.svm = data.frame(NR_true, LR_true, RR_true, NR_pred_svm, LR_pred_svm, RR_pred_svm)
  auc.svm = multi_roc(data.svm)
  auc[i,4] = auc.svm$AUC$svm$micro
  ###################################### Random Forest ####################################
  set.seed(7)
  rf = train(RapidRelapse~., data=dataTrain, method = 'rf',trControl=control, metric = 'logLoss',tuneLength = 10)
  
  # one-to-all auc
  fitted.results_rf = predict(rf, newdata = dataTest[,-1], type = "prob")
  NR_pred_rf = fitted.results_rf$NoRelapse
  LR_pred_rf = fitted.results_rf$LateRelapse
  RR_pred_rf = fitted.results_rf$RapidRelapse
  
  data.rf = data.frame(NR_true, LR_true, RR_true, NR_pred_rf, LR_pred_rf, RR_pred_rf)
  auc.rf = multi_roc(data.rf)
  auc[i, 5] = auc.rf$AUC$rf$micro
  ############################## Multinomial Regression ###################################
  set.seed(7)
  multinomial <- train(RapidRelapse~., data=dataTrain, method = "glmnet", trControl = control,metric = "logLoss", tuneGrid = expand.grid(alpha = 1,lambda = seq(1e-5, 0.1, length.out = 10)))
  # tuneGrid = expand.grid(alpha = 1,lambda = seq(1e-5, 0.1, length.out = 10)))
  # one-to-all roc
  fitted.results_multi = predict(multinomial, newdata = dataTest[,-1], type = "prob")
  NR_pred_Mul = fitted.results_multi$NoRelapse
  LR_pred_Mul = fitted.results_multi$LateRelapse
  RR_pred_Mul = fitted.results_multi$RapidRelapse
  
  data.mul = data.frame(NR_true, LR_true, RR_true, NR_pred_Mul, LR_pred_Mul, RR_pred_Mul)
  auc.multi = multi_roc(data.mul) 
  auc[i, 6] = auc.multi$AUC$Mul$micro
}
h2o.shutdown() # Disconnect from H2O server