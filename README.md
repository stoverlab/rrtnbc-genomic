# Machine learning predicts rapid relapse in triple negative breast cancer 

Code and simulated example data for the paper Machine learning predicts rapid relapse in triple negative breast cancer.

# System Requirements

## Hardware requirements
The code requires only a standard computer with enough RAM to support the in-memory operations.

## Software requirements
All statistical analyses were performed in R version 3.4.1.

### OS Requirements
The code has been tested on the following system:
+ macOS: High Sierra (10.13.6)

### R Dependencies
The following R packages are required:
```
caret
h2o
multiROC
```

# Instructions For Use
We used 77 significant clinical and genomic features to evaluate six modeling approaches, including multinomial logistic regression model, Artificial Neural Network (ANN), K-Nearest Neighbors (KNN), Support Vector Machine (SVM), Linear Discriminant Analysis (LDA), and Random Forest (RF).

## Description of files
- mydata.txt: simulated example data with 62 observations
- model comparison.R: code to compare model performance
- FinalSignatures_Symbol.txt and ExpressionSignatureCode.txt: code to compute gene expression signatures

## Demo
- to set up the development environment:
  - install R
  - install required R dependencies
- to demo the code
  - download the simulated example data mydata.txt
    - the simulated data contains 77 significant clinical, expression, immune, mutation, and copy number features
  - download the script model comparison.R
  - run the script to compare auc of six modeling approaches
    - the scipt contains a loop to compute auc for 10 independent runs
    - the typical run time for a MacBook Air with 4GB memory is around 25 minutes

## Expected output
THe expected output is a data frame of 6 columns and 10 rows. Each row is the auc of the six models for one independent run.

